/**
 * Created by Szczepan Czaicki
 * Date: 22.06.12
 * Time: 18:59
 */
package core
{

import flash.display.DisplayObject;

public class ChartModel
{

	public var title:String;
	public var params:Array;
	public var defaultValues:Array;
	public var scheme:Class;
	public var time:Array;
	public var position:Array;
	public var seriesNames:Array = [];
	public var restriction:Function;
	public var precalculate:Function;
	public var calculate:Function;
    public var secondaryParameters:Function;

	// zmienne przechowujące parametry wybrane przez użytkownika, nie trzeba ich ustawiać ręcznie w modelu
	public var paramsValues:Array;
	public var positionValues:Array;
	public var timeValue:Number;

}
}
