/**
 * Created by Szczepan Czaicki
 * Date: 23.06.12
 * Time: 23:43
 */
package core
{
import com.wis.math.alg.Complex;

import flash.utils.Dictionary;

import mx.utils.NameUtil;

import spark.core.MaskType;

//import mx.utils.NameUtil;

public class InitializeModels
{
	public function init():Array
	{
		var models:Array = [model1(), model2(), model3(), model4()]
		return models
	}

	// ==============================================================
	// =====================				 model 1				====================
	// ==============================================================
	protected function model1():ChartModel
	{
        /*
         * Model 1 - analiza wymuszenia sinusoidalnego ze znanymi parametrami pierwotnymi.
         */
		var model:ChartModel = new ChartModel()
		model.title = 'Odpowiedź na wymuszenie sinusoidalne'
		model.params = ['f [Hz]', 'l [m]',
            'R0 [Ω/km]', 'L0 [mH/km]',
            'C0 [uF/km]', 'G0 [nS/km]',
            'Z1 re', 'Z1 im',
            'Z2 re', 'Z2 im',
            'U1 amp', 'U1 fi']
		model.paramsValues = [50, 6000000, .1, 1.11, .01, 100]
		model.scheme = SchemeLibrary.scheme_test
		model.time = [0, 10000]
		model.position = [0, 10000]
		model.seriesNames = ['napięcie', 'napięcie bieżące', 'napięcie odbite']
		model.restriction = restrictions
		model.precalculate = precalculate
		model.calculate = calculate
        model.secondaryParameters = secondaryParameters;
		return model
	}

	protected function restrictions(f:Number, l:Number, R:Number, L:Number, C:Number, G:Number, ...rest):*
	{
        var c:Number = 299792458; // predkosc swiatla w m/s
        if (R <= 0 || L <= 0 || C <= 0 || G <= 0)
            return 'Niepoprawne parametry';
        if (l >= c/f/4)
            return true;
		return 'Parametry nie spepłniają warunku na linię długą';
	}

    protected function secondaryParameters(f:Number, l:Number, R:Number, L:Number, C:Number, G:Number, Z1_re:Number, Z1_im:Number,
                                           Z2_re:Number, Z2_im:Number, U1_a:Number, U1_fi:Number):Dictionary
    {
        /*
         * w - pulsacja
         * alpha - stała tłumienia
         * beta - stała fazowa
         * gamma - stała propagacji
         * Zc - impedancja falowa
         */
        var w:Number, Zc:Complex, alpha:Number, beta:Number, gamma:Complex, Zc_m:Number, Zc_fi:Number;
        var ret:Dictionary = new Dictionary();
        R *= Math.pow(10, -3);
        L *= Math.pow(10, -6);
        C *= Math.pow(10, -9);
        G *= Math.pow(10, -12);
        w = 2 * Math.PI * f;
        alpha = Math.sqrt(1/2 * (R*G - w*w*L*C + Math.sqrt((R*R + w*w * L*L) * (G*G + w*w * C*C))));
        beta = Math.sqrt(1/2 * (w*w*L*C - R*G + Math.sqrt((R*R + w*w * L*L) * (G*G + w*w * C*C))));
        gamma = new Complex(alpha, beta);
        ret['w'] = w;
        ret['alpha'] = alpha;
        ret['beta'] = beta;
        ret['gamma'] = gamma;
        Zc = Complex.root(Complex.div(new Complex(R,  w*L), new Complex(G,  w*C)), new Complex(2, 0));
        ret['Zc'] = Zc;
        return ret;
    }

    protected function precalculate(f:Number, l:Number, R:Number, L:Number, C:Number, G:Number, Z1_re:Number, Z1_im:Number,
                                    Z2_re:Number, Z2_im:Number, U1_a:Number, U1_fi:Number):Array
    {
        /*
         * Zwraca listę stałych potrzebnych do obliczenia przebiegu
         * Umb, Umo, w, Sigm1, Sigm2, alpha, beta
         */
        var secondaryParams:Dictionary = secondaryParameters(f, l,  R,  L,  C,  G,  Z1_re, Z1_im, Z2_re, Z2_im, U1_a, U1_fi);
        var U1:Complex, I1:Complex, Z1:Complex, Zc:Complex;
        var A1:Complex, A2:Complex;
        var Sigm1:Number,  Sigm2:Number;
        var Umb:Number, Umo:Number;
        U1 = new Complex(U1_a/Math.sqrt(2) * Math.cos(U1_fi), U1_a/Math.sqrt(2) * Math.sin(U1_fi));
        Z1 = new Complex(Z1_re,  Z1_im);
        Zc = secondaryParams['Zc'];
        I1 = Complex.div(U1, Z1);
        A1 = Complex.div(Complex.adds(U1,  Complex.mult(Zc,  I1)), new Complex(2,0));
        A2 = Complex.div(Complex.subt(U1,  Complex.mult(Zc,  I1)), new Complex(2,0));
        Sigm1 = Complex.arg(A1.r,  A1.i);
        Sigm2 = Complex.arg(A2.r,  A2.i);
        Umb = Math.sqrt(2) * Complex.rAbs(A1);
        Umo = Math.sqrt(2) * Complex.rAbs(A2);
        return [Umb,  Umo,
            secondaryParams['w'],
            Sigm1,  Sigm2,
            secondaryParams['alpha'],
            secondaryParams['beta']
        ];
    }

	protected function calculate(t:Number, x:Number, subresults:Array = null):Array
	{
        var     u:Number, ub:Number,  uo:Number;
        var     Umb:Number = subresults[0],
                Umo:Number = subresults[1],
                w:Number = subresults[2],
                Sigm1:Number = subresults[3],
                Sigm2:Number = subresults[4],
                alpha:Number = subresults[5],
                beta:Number = subresults[6];
        t /= 1000;
        ub = Umb * Math.pow(Math.E, -alpha*x)  * Math.sin(w*t + Sigm1 - beta*x);
        uo = Umo * Math.pow(Math.E, alpha*x)  * Math.sin(w*t + Sigm2 + beta*x);
		u = ub + uo;
		return [u, ub, uo];
	}

	// ==============================================================
	// =====================				 model 2				====================
	// ==============================================================
	protected function model2():ChartModel
	{
        /*
         * Model 2 - analiza wymuszenia sinusoidalnego ze znanymi napięciami i prądami na początku i końcu linii
         */
        var model:ChartModel = new ChartModel()
        model.title = 'Odpowiedź na wymuszenie sinusoidalne'
        model.params = ['f [Hz]', 'l [m]',
            'Z1 re', 'Z1 im',
            'Z2 re', 'Z2 im',
            'U1 amp', 'U1 fi',
            'I1 amp', 'I1 fi',
            'U2 amp', 'U2 fi',
            'I2 amp', 'I2 fi'
        ]
        model.scheme = SchemeLibrary.scheme_test
        model.time = [0, 10000]
        model.position = [0, 10000]
		model.seriesNames = ['napięcie']
        model.restriction = restrictions
        model.precalculate = precalculate2
        model.calculate = calculate
        model.secondaryParameters = secondaryParameters2;
        return model
	}

    protected function secondaryParameters2(f:Number, l:Number,
                                            Z1_re:Number, Z1_im:Number, Z2_re:Number, Z2_im:Number,
                                            U1_a:Number, U1_fi:Number, I1_a:Number, I1_fi:Number,
                                            U2_a:Number, U2_fi:Number, I2_a:Number, I2_fi:Number):Dictionary
    {
        /*
         * w - pulsacja
         * alpha - stała tłumienia
         * beta - stała fazowa
         * gamma - stała propagacji
         * Zc - impedancja falowa
         */
        var w:Number, Zc:Complex, alpha:Number, beta:Number, gamma:Complex, Zc_m:Number, Zc_fi:Number;
        var ret:Dictionary = new Dictionary();

        return ret;
    }

    protected function precalculate2(f:Number, l:Number,
                                     Z1_re:Number, Z1_im:Number, Z2_re:Number, Z2_im:Number,
                                     U1_a:Number, U1_fi:Number, I1_a:Number, I1_fi:Number,
                                     U2_a:Number, U2_fi:Number, I2_a:Number, I2_fi:Number):Array
    {
        /*
         * Zwraca listę stałych potrzebnych do obliczenia przebiegu
         * Umb, Umo, w, Sigm1, Sigm2, alpha, beta
         */
        var secondaryParams:Dictionary = secondaryParameters2(f, l,  Z1_re, Z1_im, Z2_re, Z2_im,
                U1_a, U1_fi, I1_a, I1_fi, U2_a, U2_fi, I2_a, I2_fi);
        return [];
    }


    // ==============================================================
	// =====================				 model 3				====================
	// ==============================================================
	protected function model3():ChartModel
	{
		var model:ChartModel = new ChartModel()
		model.title = 'Impedancja wejściowa linii bezstratnej'
		model.params = ['f [Hz]', 'l [m]',
            'L0 [mH/km]',
            'C0 [uF/km]',
            'Zk_re', 'Zk_im'
        ]
        model.paramsValues = [50, 6000000, 1.11, .01]
		model.scheme = SchemeLibrary.scheme_impedance
		model.time = [0, 100]
		model.position = [-100, 100]
        model.restriction = restrictions
		model.seriesNames = ['moduł imp. wejściowej']
		model.precalculate = precalculate3
		model.calculate = calculate3
        model.secondaryParameters = secondaryParameters3;
		return model
	}

    protected function secondaryParameters3(f:Number, l:Number,
                                            L:Number, C:Number,
                                            Zk_re:Number, Zk_im:Number):Dictionary
    {
        /*
         * gamma - stała propagacji
         * Z0 - impedancja linii
         * Zk - impedancja na końcu linii
         */
        var w:Number, Zc:Complex, alpha:Number, beta:Number, gamma:Complex, Zc_m:Number, Zc_fi:Number;
        var Z0:Complex;
        var ret:Dictionary = new Dictionary();
        var R:Number= 0, G:Number = 0;
        w = 2*Math.PI*f;
        gamma = Complex.root(Complex.mult(new Complex(R, w*L), new Complex(G, w*C)), new Complex(2, 0));
        ret['gamma'] = gamma;
        Z0 = Complex.root(Complex.div(new Complex(R,  w*L), new Complex(G,  w*C)), new Complex(2, 0));
        ret['Z0'] = Z0;
        return ret;
    }

	protected function precalculate3(f:Number, l:Number,
                                     L:Number, C:Number,
                                     Zk_re:Number, Zk_im:Number):Array
	{
        var Zk:Complex;
        var secondaryParams:Dictionary = secondaryParameters3(f, l,  L,  C,  Zk_re,  Zk_im);
        Zk = new Complex(Zk_re,  Zk_im);
		return [secondaryParams['Z0'], Zk, secondaryParams['gamma']];
	}

	protected function calculate3(t:Number, x:Number, subresults:Array = null):Array
	{
        var Zwe:Complex;
        var Z0:Complex = subresults[0];
        var Zk:Complex = subresults[1];
        var tan:Complex;
        var gamma:Complex = subresults[2];
        tan = Complex.tanh(Complex.mult(gamma,  new Complex(x, 0)));
        Zwe = Complex.mult(Z0,  Complex.div(Complex.adds(Zk, Complex.mult(Z0, tan)), Complex.adds(Z0,  Complex.mult(Zk,  tan))));
		return [Complex.rAbs(Zwe)];
	}

	// ==============================================================
	// =====================				 model 4				====================
	// ==============================================================
	protected function model4():ChartModel
	{
		var model:ChartModel = new ChartModel()
		model.title = 'Impedancja wejściowa linii stratnej'
        model.params = ['f [Hz]', 'l [m]',
            'R0 [Ω/km]', 'L0 [mH/km]',
            'C0 [uF/km]', 'G0 [nS/km]',
            'Zk_re', 'Zk_im'
        ]
        model.paramsValues = [50, 6000000, 0.1, 1.11, .01, 100]
		model.scheme = SchemeLibrary.scheme_impedance
		model.time = [0, 100]
		model.position = [-100, 100]
        model.restriction = restrictions
		model.seriesNames = ['moduł imp. wejściowej']
		model.precalculate = precalculate4
		model.calculate = calculate4
        model.secondaryParameters = secondaryParameters4;
		return model
	}

    protected function secondaryParameters4(f:Number, l:Number,
                                            R:Number, L:Number,
                                            C:Number, G:Number,
                                            Zk_re:Number, Zk_im:Number):Dictionary
    {
        /*
         * gamma - stała propagacji
         * Z0 - impedancja linii
         * Zk - impedancja na końcu linii
         */
        var w:Number, Zc:Complex, alpha:Number, beta:Number, gamma:Complex, Zc_m:Number, Zc_fi:Number;
        var Z0:Complex;
        var ret:Dictionary = new Dictionary();
        w = 2*Math.PI*f;
        gamma = Complex.root(Complex.mult(new Complex(R, w*L), new Complex(G, w*C)), new Complex(2, 0));
        ret['gamma'] = gamma;
        Z0 = Complex.root(Complex.div(new Complex(R,  w*L), new Complex(G,  w*C)), new Complex(2, 0));
        ret['Z0'] = Z0;
        return ret;
    }

    protected function precalculate4(f:Number, l:Number,
                                     R:Number, L:Number,
                                     C:Number, G:Number,
                                     Zk_re:Number, Zk_im:Number):Array
    {
        var Zk:Complex;
        var secondaryParams:Dictionary = secondaryParameters4(f, l,  R, L, C, G, Zk_re,  Zk_im);
        Zk = new Complex(Zk_re,  Zk_im);
        return [secondaryParams['Z0'], Zk, secondaryParams['gamma']];
    }

    protected function calculate4(t:Number, x:Number, subresults:Array = null):Array
    {
        var Zwe:Complex;
        var Z0:Complex = subresults[0];
        var Zk:Complex = subresults[1];
        var tan:Complex;
        var gamma:Complex = subresults[2];
        tan = Complex.tanh(Complex.mult(gamma,  new Complex(x, 0)));
        Zwe = Complex.mult(Z0,  Complex.div(Complex.adds(Zk, Complex.mult(Z0, tan)), Complex.adds(Z0,  Complex.mult(Zk,  tan))));
        return [Complex.rAbs(Zwe)];
    }

	// ==============================================================
	// =====================				 mock				====================
	// ==============================================================
	protected function mockFunction(...rest):Dictionary
	{
		return new Dictionary()
	}
}
}
