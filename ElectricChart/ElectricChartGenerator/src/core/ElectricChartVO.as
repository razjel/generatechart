/**
 * Created by Szczepan Czaicki
 * Date: 24.06.12
 * Time: 00:36
 */
package core
{

public class ElectricChartVO
{

	public var time:Number;
	public var values:Array;

	public function ElectricChartVO(time:Number, values:Array)
	{
		this.time = time
		this.values = values
	}

	public function get value0():Number { return values[0] }
	public function get value1():Number { return values[1] }
	public function get value2():Number { return values[2] }
	public function get value3():Number { return values[3] }
	public function get value4():Number { return values[4] }
	public function get value5():Number { return values[5] }
	public function get value6():Number { return values[6] }
	public function get value7():Number { return values[7] }
	public function get value8():Number { return values[8] }
	public function get value9():Number { return values[9] }
}
}
