/**
 * Created by Szczepan Czaicki
 * Date: 22.06.12
 * Time: 19:06
 */
package core
{

public class SchemeLibrary
{

	[Embed(source="/res/schemes/odcinek.png")]
	public static var scheme_test:Class;

    [Embed(source="/res/schemes/impeda1.gif")]
    public static var scheme_impedance:Class;
}
}
