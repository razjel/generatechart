/**
 * Created by Michal Czaicki, mczaicki @ gmail.com
 * Date: 30.03.12
 */
package com.mlib.events
{
	import flash.events.Event;

	public class CloneEvent extends Event
	{
		public function CloneEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			var cl:Class = Object(this).constructor
			var e:CloneEvent= new cl(type, bubbles, cancelable)
			return e;
		}
	}
}
