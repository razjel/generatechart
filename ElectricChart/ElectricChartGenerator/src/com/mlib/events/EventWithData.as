/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License 
 * in root directory in mlib_license.txt file or at
 * http://www.mozilla.org/MPL/
 * 
 */
/*
 * Created by Michal Czaicki, mczaicki|at|gmail.com
 * Date: 05.01.2011
 */

package com.mlib.events
{

	import flash.events.Event;

	public class EventWithData extends CloneEvent
	{
		public var data:Object;

		public function EventWithData(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}

		override public function clone():Event
		{
			var e:EventWithData = EventWithData(super.clone());
			e.data = data;
			return e;
		}
	}
}